package inputOutput

import inputOut.InputProcessor
import spock.lang.Subject
import spock.lang.Title

/**
 * Created by Golden
 */
@Title("Unit Tests For InputProcessor")
@Subject(InputProcessor)
class InputProcessorSpec extends spock.lang.Specification{

    InputProcessor inputProcessor = new InputProcessor()
    String input;
    String response;

    def'Should Map Intergalactic Unit To Roman Numeral If Input Line Matches A MAP_UNITS Regex'(){
        given:"A String That Matches A MAP_UNITS Regex"
        input = "glob is I"

        when:"processInput Is Called"
        inputProcessor.processInput(input, "")

        then:'The Intergalactic Unit And Roman Numeral Value Should Be Added To The Map'
        inputProcessor.manageIntergalacticUnits.intergalacticToRomanNumeralsMap.containsKey("glob")
        inputProcessor.manageIntergalacticUnits.intergalacticToRomanNumeralsMap.containsValue((char)'I')
    }

    def'Should Compute Save Metal And Its Value'(){
        given:"A String That Matches A METAL_CREDIT_DATA Regex"
        input = "glob glob Silver is 34 Credits"
        inputProcessor.manageIntergalacticUnits.addIntergalacticUnits("glob", (char)'I')

        when:"processInput Is Called"
        inputProcessor.processInput(input, "")

        then:'The Metal And Roman Value Should Be Added To The Map'
        inputProcessor.manageMetalCredits.intergalacticToRomanMap.containsKey("Silver")
        inputProcessor.manageMetalCredits.intergalacticToRomanMap.containsValue((double)17)
    }

    def'Should Calculate The Numeric Value Of The Intergalactic Units In Within The Question "How Much"'(){
        given:'A String Starting With "How much is "'
        input = "how much is pish tegj glob glob ?"
        populateUnits()

        when:"processInput Is Called"
        response =inputProcessor.processInput(input, "")

        then:'An Appropriate Answer Should Be Given'
        response.equals("pish tegj glob glob is 42")
    }

    def'Should Calculate The Credit Value Of The Metal Within The Question "How Many"'(){
        given:'A String Starting With "How many Credits "'
        input = "how many Credits is glob prok Silver ?"

        populateUnits()

        when:"processInput Is Called"
        response =inputProcessor.processInput(input, "")

        then:'An Appropriate Answer Should Be Given'
        response.equals("glob prok Silver is 68 Credits")
    }

    private void populateUnits() {
        inputProcessor.manageIntergalacticUnits.addIntergalacticUnits("glob", (char)'I')
        inputProcessor.manageIntergalacticUnits.addIntergalacticUnits("prok", (char)'V')
        inputProcessor.manageIntergalacticUnits.addIntergalacticUnits("pish", (char)'X')
        inputProcessor.manageIntergalacticUnits.addIntergalacticUnits("tegj", (char)'L')
        inputProcessor.manageMetalCredits.addMetalToMap("Silver", (double)17)
    }
}
