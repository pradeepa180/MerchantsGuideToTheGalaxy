package inputOutput

import inputOut.ConversationNotesReader
import inputOut.ConversationNotesReaderRuntimeException
import service.numeral.management.ManageIntergalacticUnits
import spock.lang.Subject
import spock.lang.Title

import java.nio.file.Path
import java.nio.file.Paths

/**
 * Created by Golden
 */
@Title("Unit Tests For ConversationNotesReader")
@Subject(ConversationNotesReader)
class ConversationNotesReaderSpec extends spock.lang.Specification{

    ConversationNotesReader conversationNotesReader;

    def'Should Create List Of Strings From File'(){
        given:'A Path'
        Path filePath = Paths.get(String.format("%s/src/test/resources/GalaxyConversationNotesTest.txt",
                                        Paths.get("").toAbsolutePath().toString()));
        conversationNotesReader = new ConversationNotesReader(filePath);

        when:'readNotesFromFile is called'
        List<String> conversationNotes = conversationNotesReader.readNotesFromFile();

        then:'The Returned List Should Contain All Lines In The File'
        conversationNotes.equals(getConversationNotes())
    }

    def'Should Throw A ConversationNotesReaderRuntimeException If Given File Path Is Not Found'(){
        given:'A Path'
        Path filePath = Paths.get(String.format("%s/some/weird/path/leading/nowhere",
                Paths.get("").toAbsolutePath().toString()));
        conversationNotesReader = new ConversationNotesReader(filePath);

        when:'readNotesFromFile is called'
        conversationNotesReader.readNotesFromFile();

        then:'A ConversationNotesReaderRuntimeException Should Be Raised'
        thrown(ConversationNotesReaderRuntimeException)
    }

    private List<String> getConversationNotes(){
        List<String> conversationNotes = new ArrayList<>()
        conversationNotes.add("glob is I");
        conversationNotes.add("prok is V");
        conversationNotes.add("pish is X");
        conversationNotes.add("tegj is L");
        conversationNotes.add("glob glob Silver is 34 Credits");
        conversationNotes.add("glob prok Gold is 57800 Credits");
        conversationNotes.add("pish pish Iron is 3910 Credits");
        conversationNotes.add("how much is pish tegj glob glob ?");
        conversationNotes.add("how many Credits is glob prok Silver ?");
        conversationNotes.add("how many Credits is glob prok Gold ?");
        conversationNotes.add("how many Credits is glob prok Iron ?");
        conversationNotes.add("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?");
        return conversationNotes
    }
}
