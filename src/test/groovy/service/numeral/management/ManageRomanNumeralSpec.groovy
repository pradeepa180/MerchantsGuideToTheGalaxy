package service.numeral.management

import spock.lang.Subject
import spock.lang.Title
import spock.lang.Unroll

/**
 * Created by golden.
 */
@Title("ManageRomanNumeral Unit Tests")
@Subject(ManageRomanNumeral)
class ManageRomanNumeralSpec extends spock.lang.Specification{

    ManageRomanNumeral manageRomanNumeral = new ManageRomanNumeral()
    String romanNumeral
    int actualResult

    def setup(){
        actualResult = 0
        romanNumeral = ""
    }

    @Unroll('baseSymbol #baseSymbol, expectedResult #expectedResult')
    def'Should Convert The Seven Known Roman Symbols To Numbers.'(){

    given:'A Roman Numeral Symbol between  I and M'
    String romanNumeral = baseSymbol

    when:'convertRomanNumeralToNumber is called'
    actualResult = manageRomanNumeral.convertRomanNumeralToNumber(romanNumeral)

    then:'The Correct Value Of The Base Symbol Should Be Returned'
    actualResult == expectedResult

    where: 'The Various Input Symbols Are'
    baseSymbol | expectedResult
    "I" | 1
    "V" | 5
    "X" | 10
    "L" | 50
    "C" | 100
    "D" | 500
    "M" | 1000
    }

    @Unroll('combinedSymbols #combinedSymbols, expectedResult #expectedResult')
    def'Should Convert A Roman Numeral With Combined Symbols By Adding The Values Together'(){
        given: 'Roman Numeral With Combined Symbols'
        romanNumeral = combinedSymbols

        when:'convertRomanNumeralToNumber is called'
        actualResult = manageRomanNumeral.convertRomanNumeralToNumber(romanNumeral)

        then:'The Correct Value Of The Base Symbol Should Be Returned'
        actualResult == expectedResult

        where: 'The Various Inputs of Combined Symbols Are'
        combinedSymbols | expectedResult
        "MMVI" | 2006
        "VI"   | 6
        "III"     | 3
    }

    @Unroll('combinedSymbols #combinedSymbols, actualResult #expectedResult')
    def'Should Convert A Roman Numeral With Combined Symbols By Adding The Values Together, And Subtracting Smaller Values from Larger Values'(){
        given: 'Roman Numeral With Combined Symbols'
        romanNumeral = combinedSymbols

        when:'convertRomanNumeralToNumber is called'
        actualResult = manageRomanNumeral.convertRomanNumeralToNumber(romanNumeral)

        then:'The Correct Value Of The Base Symbol Should Be Returned'
        actualResult == expectedResult

        where: 'The Various Inputs of Combined Symbols Are'
        combinedSymbols | expectedResult
        "MCMXLIV" | 1944
        "IV"   | 4
        "IX"     | 9
    }

    def'Should Not Convert A Roman Numeral With A Subtraction Violaation Rule'(){
        given:'A Roman Numeral That Violates One Of the Subtraction Rules'
        romanNumeral = someIllegalRomanNumeral

        when:'convertRomanNumeralToNumber is called'
        manageRomanNumeral.convertRomanNumeralToNumber(romanNumeral)

        then: 'A Validation Exception Should Be Raised'
        thrown(SymbolSubtractionRuntimeException)

        where: 'The Various Inputs of Combined Symbols Violating One Of the Subtraction Rules Are'
        someIllegalRomanNumeral | exception
        "IM" | SymbolSubtractionRuntimeException
        "XD" | SymbolSubtractionRuntimeException
        "VX" | SymbolSubtractionRuntimeException
        "LC" | SymbolSubtractionRuntimeException
        "DM" | SymbolSubtractionRuntimeException
    }

}
