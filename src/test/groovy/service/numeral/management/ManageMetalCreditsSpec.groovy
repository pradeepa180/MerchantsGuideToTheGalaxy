package service.numeral.management

import spock.lang.Subject
import spock.lang.Title

/**
 * Created by Golden
 */
@Title("Unit Tests For ManageMetalCredits")
@Subject(ManageMetalCredits)
class ManageMetalCreditsSpec extends spock.lang.Specification{

    ManageIntergalacticUnits manageIntergalacticUnits = new ManageIntergalacticUnits()
    ManageMetalCredits manageMetalCredits = new ManageMetalCredits(manageIntergalacticUnits)

    def'Should Compute The Credit Value Given Intergalactic Units and Credit Information'(){
        given:'Intergalactic Units And Credits For A Metal'
        String intergalacticUnits = "glob glob"
        int credits = 34
        manageMetalCredits.manageIntergalacticUnits.addIntergalacticUnits("glob", (char)'I')

        when:'computeSingleMetalCreditValue is called'
        double  response = manageMetalCredits.computeSingleMetalCreditValue(intergalacticUnits.split(" ").toList(), credits)

        then:'A Correct Computed Value For The Metal Credit Should Be Returned'
        response == 17
    }

    def'Should Add New Metal And Value To Map'(){
        given:'A Metal And Corresponding Value'
        String metal = "Silver"
        int value = 17

        when:'addMetalToMap Is Called'
        manageMetalCredits.addMetalToMap(metal, value)

        then:'The New Metal And Value Should Be Added To The Map'
        manageMetalCredits.intergalacticToRomanMap.containsKey(metal) == true
    }
}
