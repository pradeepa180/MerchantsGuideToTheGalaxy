package service.question.management

import service.numeral.management.ManageIntergalacticUnits
import service.numeral.management.ManageMetalCredits
import service.numeral.management.ManageRomanNumeral
import service.numeral.management.ManageRomanNumeralRules
import spock.lang.Subject
import spock.lang.Title

/**
 * Created by Golden
 */
@Title("Unit Tests For ManageQuestions")
@Subject(ManageQuestions)
class ManageQuestionsSpec extends spock.lang.Specification{

    ManageMetalCredits manageMetalCredits = new ManageMetalCredits()
    ManageIntergalacticUnits manageIntergalacticUnits = new ManageIntergalacticUnits()
    ManageQuestions manageQuestions = new ManageQuestions(manageMetalCredits, manageIntergalacticUnits)

    def setup(){
        manageQuestions.manageIntergalacticUnits.addIntergalacticUnits("glob", (char)'I')
        manageQuestions.manageIntergalacticUnits.addIntergalacticUnits("prok", (char)'V')
        manageQuestions.manageIntergalacticUnits.addIntergalacticUnits("pish", (char)'X')
        manageQuestions.manageIntergalacticUnits.addIntergalacticUnits("tegj", (char)'L')
        manageQuestions.manageMetalCredits.addMetalToMap("Silver", 17)
    }

    def'Should Calculate The Numeric Value Of The Intergalactic Units In Within The Question'(){
        given:'A String Starting With "How much is "'
        String howMuchQuestion = "How much is pish tegj glob glob ?"

        when:'computeTheHowMuchQuestion is called'
        String response = manageQuestions.computeTheHowMuchQuestion(howMuchQuestion)

        then:'An Appropriate Answer Should Be Given'
        response.equals("pish tegj glob glob is 42")
    }

    def'Should Calculate The Numeric Value Of The Intergalactic Units In Within The Questions'(){
        given:'A String Starting With "How much is "'
        String howMuchQuestion = "how much wood could a woodchuck chuck if a woodchuck could chuck wood ?"

        when:'computeTheHowMuchQuestion is called'
        String response = manageQuestions.computeTheHowMuchQuestion(howMuchQuestion)

        then:'An Appropriate Answer Should Be Given'
        response.equals(manageQuestions.unKnownRequestResponse)
    }

    def'Should Not Calculate The Numeric Value Of The Intergalactic Units Violating Repetition Rule'(){
        given:'A String Starting With "How much is "'
        String howMuchQuestion = "How much is pish pish pish pish ?"

        when:'computeTheHowMuchQuestion is called'
        String response = manageQuestions.computeTheHowMuchQuestion(howMuchQuestion)

        then:'An Appropriate Answer Should Be Given'
        response.equals(manageQuestions.unKnownRequestResponse)
    }

    def'Should Not Calculate The Numeric Value Of The Intergalactic Units Violating Subtraction Rule'(){
        given:'A String Starting With "How much is "'
        String howMuchQuestion = "How much is prok tegj pish pish ?"

        when:'computeTheHowMuchQuestion is called'
        String response = manageQuestions.computeTheHowMuchQuestion(howMuchQuestion)

        then:'An Appropriate Answer Should Be Given'
        response.equals(manageQuestions.unKnownRequestResponse)
    }

    def'Should Calculate The Credit Value Of The Metal Within The Question'(){
        given:'A String Starting With "How many Credits "'
        String howMuchQuestion = "how many Credits is glob prok Silver ?"

        when:'computeTheHowMuchQuestion is called'
        String response = manageQuestions.computeTheHowManyQuestion(howMuchQuestion)

        then:'An Appropriate Answer Should Be Given'
        response.equals("glob prok Silver is 68 Credits")
    }

    def'Should Not Calculate The Credit Value Of The Metal Within The Question If Repetition Rule Is Violated '(){
        given:'A String Starting With "How many Credits "'
        String howMuchQuestion = "how many Credits is tegj tegj Silver ?"

        when:'computeTheHowMuchQuestion is called'
        String response = manageQuestions.computeTheHowManyQuestion(howMuchQuestion)

        then:'An Appropriate Answer Should Be Given'
        response.equals(manageQuestions.unKnownRequestResponse)
    }

    def'Should Not Calculate The Credit Value Of The Metal Within The Question If Subtraction Rule Is Violated '(){
        given:'A String Starting With "How many Credits "'
        String howMuchQuestion = "how many Credits is prok pish Silver ?"

        when:'computeTheHowMuchQuestion is called'
        String response = manageQuestions.computeTheHowManyQuestion(howMuchQuestion)

        then:'An Appropriate Answer Should Be Given'
        response.equals(manageQuestions.unKnownRequestResponse)
    }

    def'Should Not Calculate The Credit Value Of The Metal Within The Question If Metal Is Not Found'(){
        given:'A String Starting With "How many Credits "'
        String howMuchQuestion = "how many Credits is glob pish Gold ?"

        when:'computeTheHowMuchQuestion is called'
        String response = manageQuestions.computeTheHowManyQuestion(howMuchQuestion)

        then:'An Appropriate Answer Should Be Given'
        response.equals(manageQuestions.unKnownRequestResponse)
    }


    def'Should Not Calculate The Credit Value Of The Metal Within The Question If Symbol Is Not Found'(){
        given:'A String Starting With "How many Credits "'
        String howMuchQuestion = "how many Credits is unKnow pish Silver ?"

        when:'computeTheHowMuchQuestion is called'
        String response = manageQuestions.computeTheHowManyQuestion(howMuchQuestion)

        then:'An Appropriate Answer Should Be Given'
        response.equals(manageQuestions.unKnownRequestResponse)
    }

}