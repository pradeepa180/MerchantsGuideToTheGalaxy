package service.numeral.management;

/**
 * Created by Golden
 */
public class SymbolSubtractionRuntimeException extends RuntimeException {

    public SymbolSubtractionRuntimeException(String message) {
        super(message);
    }
}
