package service.numeral.management;

import domain.RomanNumeral;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by golden.
 */
public class ManageRomanNumeralRules {

    public boolean isSymbolRepeatedCorrectly(String romanNumeralSymbols){

        Pattern repeatedSymbolViolation;
        Matcher matcher;
        for(RomanNumeral romanNumeral: RomanNumeral.values()){
            repeatedSymbolViolation = Pattern.compile(romanNumeral.getRepetitionsNotAllowed());
            matcher = repeatedSymbolViolation.matcher(romanNumeralSymbols);
            if (matcher.find()){
                return false;
            }
        }
        return true;
    }

    public boolean isSubtractionRuleViolated(char firstRomanSymbol, char nextRomanSymbol){
        String firstRomanSymbolAsString = String.valueOf(firstRomanSymbol);
        String nextRomanSymbolAsString = String.valueOf(nextRomanSymbol);
        String errorMessage = " cannot be subtracted from";
        String errorSymbolMessage = "Symbol ";

        if(firstRomanSymbolAsString.equals(RomanNumeral.V.toString()) || firstRomanSymbolAsString.equals(RomanNumeral.L.toString())
                || firstRomanSymbolAsString.equals(RomanNumeral.D.toString())){
            throw new SymbolSubtractionRuntimeException(errorSymbolMessage + firstRomanSymbol + errorMessage + nextRomanSymbol);
        }

        switch (firstRomanSymbol){
            case 'I':
                if (!(nextRomanSymbolAsString.equals(RomanNumeral.V.toString())) && !(nextRomanSymbolAsString.equals(RomanNumeral.X.toString()))){
                    throw new SymbolSubtractionRuntimeException(errorSymbolMessage + firstRomanSymbol + errorMessage + nextRomanSymbol);
                }
                break;
            case 'X':
                if (!(nextRomanSymbolAsString.equals(RomanNumeral.L.toString())) && !(nextRomanSymbolAsString.equals(RomanNumeral.C.toString()))){
                    throw new SymbolSubtractionRuntimeException(errorSymbolMessage + firstRomanSymbol + errorMessage + nextRomanSymbol);
                }
                break;
            case 'C':
                if (!(nextRomanSymbolAsString.equals(RomanNumeral.D.toString())) && !(nextRomanSymbolAsString.equals(RomanNumeral.M.toString()))){
                    throw new SymbolSubtractionRuntimeException(errorSymbolMessage + firstRomanSymbol + errorMessage + nextRomanSymbol);
                }
                break;
        }
        return false;
    }
}
