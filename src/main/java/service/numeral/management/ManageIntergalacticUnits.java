package service.numeral.management;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Golden
 */
public class ManageIntergalacticUnits {

    Map<String, Character> intergalacticToRomanNumeralsMap;

    public ManageIntergalacticUnits(){
        this.intergalacticToRomanNumeralsMap = new HashMap<String, Character>();
    }

    public String convertIntergalacticUnitsToRomanNumerals(List<String> intergalacticUnits){
        String romanNumeral = "";

        for(String intergalacticUnit: intergalacticUnits){
            if(!intergalacticToRomanNumeralsMap.containsKey(intergalacticUnit)){
                throw new SymbolNotFoundRunException("Cannot Find Symbol " + intergalacticUnit);
            }
            romanNumeral +=intergalacticToRomanNumeralsMap.get(intergalacticUnit);
        }

        return  romanNumeral;
    }

    public void addIntergalacticUnits(String intergalacticUnit, char romanNumeral){
        intergalacticToRomanNumeralsMap.put(intergalacticUnit, romanNumeral);
    }

    public Map<String, Character> getIntergalacticToRomanNumeralsMap() {
        return intergalacticToRomanNumeralsMap;
    }
}
