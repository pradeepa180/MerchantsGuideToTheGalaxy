package service.numeral.management;

/**
 * Created by Golden
 */
public class SymbolNotFoundRunException extends RuntimeException {

    public SymbolNotFoundRunException(String message) {
        super(message);
    }
}
