package service.numeral.management;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Golden
 */
public class ManageMetalCredits {

    Map<String, Double> intergalacticToRomanMap;
    ManageIntergalacticUnits manageIntergalacticUnits;
    ManageRomanNumeral manageRomanNumeral;

    public ManageMetalCredits(ManageIntergalacticUnits manageIntergalacticUnits){
        this.intergalacticToRomanMap = new HashMap<String, Double>();
        this.manageIntergalacticUnits = manageIntergalacticUnits;
        this.manageRomanNumeral = new ManageRomanNumeral();
    }

    public  double computeSingleMetalCreditValue(List<String> intergalacticUnits, int credits){
       String romanNumerals = manageIntergalacticUnits.convertIntergalacticUnitsToRomanNumerals(intergalacticUnits);
       double numberValue = manageRomanNumeral.convertRomanNumeralToNumber(romanNumerals);
       return credits/numberValue;
    }

    public void addMetalToMap(String metal, double value){
        if(!intergalacticToRomanMap.containsKey(metal)){
            intergalacticToRomanMap.put(metal,value);
        }
    }

    public Map<String, Double> getIntergalacticToRomanMap(){
        return intergalacticToRomanMap;
    }

}