package inputOut;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by Golden
 */
public class ConversationNotesReader {

    private Path filePath;

    public ConversationNotesReader(Path filePath){
        this.filePath = filePath;
    }

    public List<String> readNotesFromFile(){

        List<String> conversationNotes;

        try{
            conversationNotes = Files.readAllLines(this.filePath);
        }catch (IOException e){
            throw  new ConversationNotesReaderRuntimeException("Error while reading file: " + filePath, e);
        }

        return conversationNotes;
    }
}
